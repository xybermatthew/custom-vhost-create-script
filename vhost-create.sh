#!/bin/bash

# ######################################################################
# This script, 'vhost-create.sh,' creates a new vHost on a Ubuntu 
# Server. It assumes that a '/etc/apache2/sites-available' and 
# '/etc/apache2/sites-enabled' setup is used. It's 
# recommend that you put this file and others 
# (see below list) in /usr/local/bin/scripts.
# 
# 
# This script depends on additional sub-scripts (be sure to include 
# these in the same directory):
# - sitesListing.sh         # current not automated
# - installWordPress.sh     # automates the install of WordPress
# - sitePermissions.sh      # automates file and folder permissions
# 
# 
# Author: Mateo Moore <me@mateoco.de>
# URL: http://mateoco.de/
# 
# 
# Credits, for providing similar code:
# - Fred Brooker <original@fredbrooker.cz>
# - Chris Fidao <fideloper@gmail.com>
# ######################################################################


# ######################################################################
# CHANGE THESE VARIABLES TO MATCH YOUR CONFIGURATION!
# ######################################################################
# For the Apache vhost config file
ADMIN=hostmaster@yourdomain.com

# For use with a MySQL Database
MYSQLUSER=root
MYSQLPASS=root
MYSQLHOST=localhost

# For use with a WordPress Install
MYSQLPRET=wp_

# For use in basic homepage
COMPANY="Your Company Name"
COMPANYURL=http://yourdomain.com


# ######################################################################
# !!!!!!!!!!!!!!!!! NO NEED TO EDIT BELOW THIS LINE !!!!!!!!!!!!!!!!!! #
# ######################################################################


######################################################################
# Additional variables; pre-set
# ######################################################################
# For the Apache vhost config file, MySQL Database, & Folder setup
# It grabs your systems hostname from environment variables 
HOST=${HOSTNAME} 
# For the Apache vhost config file
VHOST_CONF=/etc/apache2/sites-available
# For Folder setup
WWW_ROOT=/var/www
OWNER=www-data:www-data 
CHMOD=0755
# For MySQL Database setup
RANDPASS=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)
# For assoicated scripts
SCRIPTS=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)


# ######################################################################
# Start by clearing the terminal
# ######################################################################
clear


# ######################################################################
# Get subdomain from passed in input
# ######################################################################
function subdomain() {
    echo $DOMAIN | awk -F "." '{print $1}'
}


# ######################################################################
# Test for input, if none exit, else create subdomain
# ######################################################################
if [ $# -eq 0 ]
then
    echo "To create a virtual host you need to pass in the sub-domain or the complete domain name. Syntax: $(basename $0) sub-domain or sub-domain.domain.tld"
    exit 1
else
    DOMAIN=$1                   # get input
    SUBDOMAIN=$(subdomain)      # process input and parse it for the subdomain value
    DOMAIN=${SUBDOMAIN}.${HOST} # set up full domain for apache
    PARSEDDOMAIN=$(SUBDOMAIN)   # copy subdomain for use in apache conf
fi


# ######################################################################
# Create MySQL Database name from subdomain but replacing bad characters
# ######################################################################
function dbname()
{
    DOMAIN=$1
    CLEAN_DOMAIN=$(echo $DOMAIN | awk -F "." '{print $1}' | sed -r 's/[-]+/_/g')
    echo ${CLEAN_DOMAIN:0:15}
}
DBNAME=$(dbname $DOMAIN)


# ######################################################################
# Create MySQL Database user from subdomain but replacing bad characters
# ######################################################################
function dbuser()
{
    DOMAIN=$1
    CLEAN_DOMAIN=$(echo $DOMAIN | awk -F "." '{print $1}' | sed -r 's/[-_]+//g')
    echo ${CLEAN_DOMAIN:0:15}
}
DBUSER=$(dbuser $DOMAIN)


# ######################################################################
# Test for site folder and give feedback to user
# ######################################################################
if [ -d "$WWW_ROOT/$SUBDOMAIN" ]; then
    echo "The folder '$WWW_ROOT/$SUBDOMAIN' is already created."
    echo
else
    echo "Creating the folder '$SUBDOMAIN' at '$WWW_ROOT/$SUBDOMAIN'."
    echo
fi


# ######################################################################
# Make site folder
# ######################################################################
mkdir -p $WWW_ROOT/$SUBDOMAIN


# ######################################################################
# Chown new folder
# ######################################################################
chown $OWNER $WWW_ROOT/$SUBDOMAIN


# ######################################################################
# Chmod new folder
# ######################################################################
chmod $CHMOD $WWW_ROOT/$SUBDOMAIN


# ######################################################################
# Build Apache Virtual Host file
# ######################################################################
function create_vhost {
source vhost-apache-config-example.conf
}


# ######################################################################
# Test for Apache Virtual Host file and give feedback to user
# ######################################################################
if [ -f "$VHOST_CONF/$DOMAIN.conf" ]; then
    echo "The Apache vhost exists. Moving on…"
    echo
else
    echo "Creating the Apache vhost file."
    echo
fi


# ######################################################################
# Create Apache Virtual Host file
# ######################################################################
create_vhost > $VHOST_CONF/$DOMAIN.conf


# ######################################################################
# synchronize data on disk with memory
# ######################################################################
sync


# ######################################################################
# Enable the new site
# ######################################################################
a2ensite $DOMAIN > /dev/null


# ######################################################################
# Restart Apache
# ######################################################################
/etc/init.d/apache2 graceful > /dev/null


# ######################################################################
# Give feedback to user
# ######################################################################
echo "The site '$DOMAIN' has been enabled."
echo


# ######################################################################
# Ask if a MySQL Database should be created
# ######################################################################
while true; do
    read -p "Would you like to install a MySQL Database (yes or no)? " yn
    case $yn in
        [Yy]es* )  
            # ######################################################################
            # Create MySQL Database
            # ######################################################################
            # 
            # TODO: Test for preexisting database; see TODO.md
            # 
            mysql -h$MYSQLHOST -u$MYSQLUSER -p$MYSQLPASS -e "CREATE USER '$DBUSER'@'$MYSQLHOST' IDENTIFIED BY '$RANDPASS'; CREATE DATABASE IF NOT EXISTS $DBNAME; GRANT ALL ON $DBNAME.* TO '$DBUSER'@'$MYSQLHOST'; FLUSH PRIVILEGES;"

            # ######################################################################
            # Give feedback to user
            # ######################################################################
            # 
            # TODO: Automate updating of sitesListing.sh or find a better way; see TODO.md
            # 
            echo "################################################################################"
            echo "#IMPORTANT!!! IMPORTANT!!! IMPORTANT!!! IMPORTANT!!! IMPORTANT!!! IMPORTANT!!! #"
            echo "################################################################################"
            echo "PLEASE MAKE NOTE OF THE FOLLOWING FOR THE NEW DOMAIN '$DOMAIN':"
            echo
            echo "DB USER = '$DBNAME'"
            echo "DB PASS = '$RANDPASS'"
            echo
            echo "YOU ALSO NEED TO UPDATE THE 'sitesListing.sh' FILE WITH THE FOLLOWING:"
            echo
            echo "SITE=( '"$DOMAIN"' )"
            echo "DBHOST=( '"$MYSQLHOST"' )"
            echo "DBUSER=( '"$DBNAME"' )"
            echo "DBPASS=( '"$RANDPASS"' )"
            echo "DATABASE=( '"$DBNAME"' )"
            echo
            echo "**The above information is used to run the file permission and backup scripts!"
            echo
            break;; # end case
        [Nn]o* ) 
            # ######################################################################
            # Exit and move on
            # ######################################################################
            break;; # end case
        * ) 
            # ######################################################################
            # Entry not entered give feedback to user
            # ######################################################################
            echo "Please answer yes or no.";;
    esac # end case
done # end while


# ######################################################################
# Build simple home/landing page
# ######################################################################
function create_homepage {
cat <<- _EOF_
<!doctype html>
<html lang='en'>
<head>
<meta charset='utf-8'>
<title>$SUBDOMAIN &mdash; Another $COMPANY Production.</title>
<meta name='description' content='$SUBDOMAIN &mdash; Another $COMPANY Production.'>
<meta name='author' content='$COMPANY'>
<!--[if lt IE 9]>
<script src='//html5shiv.googlecode.com/svn/trunk/html5.js'></script>
<![endif]-->
</head>
<body>
<h1>Hello There&hellip;</h1>
<p>You have reached the home page for $SUBDOMAIN.</p>
<p>Another <a href='$COMPANYURL'>$COMPANY</a> Production.</p>
</body>
</html>
_EOF_
}


# ######################################################################
# Build .htaccess file with basic authentication 
# ######################################################################
function create_htaccess {
cat <<- _EOF_
AuthType Basic
AuthName 'Restricted Area'
AuthUserFile /root/scripts/.htpasswd
require valid-user
_EOF_
}


# ######################################################################
# Ask if WordPress should be installed
# ######################################################################
while true; do
    read -p "Would you like to install a default WordPress (yes or no)? " yn
    case $yn in
        [Yy]es* ) 
            # ######################################################################
            # Run WordPress Script
            # ######################################################################
            # 
            # TODO: Test for preexisting site folder; see TODO.md
            # 
            export DOMAIN=$DOMAIN && export SCRIPTS=$SCRIPTS && export WWW_ROOT=$WWW_ROOT && export SUBDOMAIN=$SUBDOMAIN && cd $WWW_ROOT/$SUBDOMAIN/ && exec $SCRIPTS/installWordPress.sh; 
            break;; # end case
        [Nn]o* ) 
            while true; do
            create_homepage > $WWW_ROOT/$SUBDOMAIN/index.html
                read -p "Would you like to secure this site with basic Apache authentication (yes or no)? " yn
                case $yn in
                    [Yy]es* ) 
                        # ######################################################################
                        # Test for .htaccess file and give feedback to user
                        # ######################################################################
                        if [ -f "$WWW_ROOT/$SUBDOMAIN/.htaccess" ]; then
                            echo "The .htaccess file has already been created. Moving on…"
                            echo
                        else
                            echo "Creating the .htaccess file."
                            echo
                            # ######################################################################
                            # Create .htaccess file
                            # ######################################################################
                            create_htaccess > $WWW_ROOT/$SUBDOMAIN/.htaccess

                            # ######################################################################
                            # Synchronize data on disk with memory
                            # ######################################################################
                            sync
                        fi; # end if
                        break;; # end inner case
                    [Nn]o* ) 
                        # ######################################################################
                        # Do nothing 
                        # ######################################################################
                        echo "";
                        break;; # end inner case
                    * ) 
                        # ######################################################################
                        # Entry not entered give feedback to user
                        # ######################################################################
                        echo "Please answer yes or no.";;
                esac # end inner case
            done; # end while
            break;; # end case
        * ) 
            # ######################################################################
            # Entry not entered give feedback to user
            # ######################################################################
            echo "Please answer yes or no.";;
    esac # end case
done # end while


# ######################################################################
# Run Permissions Script; passing yes variable
# ######################################################################
bash $SCRIPTS/sitePermissions.sh y


# ######################################################################
# Get Server IP Address
# ######################################################################
IP=$(curl -s icanhazip.com)


# ######################################################################
# Give feedback to user
# ######################################################################
echo "Yeah, your website has been installed. You can now view your site in a browser window at: http://$DOMAIN or http://$IP. Don't forget, you may need to add a DNS entry for this $SUBDOMAIN."


# ######################################################################
# End
# ######################################################################
exit 0
